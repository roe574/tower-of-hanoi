let playerChoice = true;
let playerChoicePicked;

function moveDisc(click) {
    let tower = click.currentTarget;

    if(playerChoice === true) {
        playerChoicePicked = tower.lastElementChild;
        document.getElementById('playerPicked').appendChild(playerChoicePicked);
        playerChoice = false;
    }
    else if(playerChoice === false) {
        if (!tower.lastElementChild) {
            tower.appendChild(playerChoicePicked);
            playerChoice = true;
        }
        else if (Number(playerChoicePicked.id) < Number(tower.lastElementChild.id)) {
            tower.appendChild(playerChoicePicked);
            playerChoice = true;
        }
    }
    
    if(document.getElementById('towerEnd').childElementCount === 4) {
        alert('Winner!!!');
    }
}

document.getElementById('towerStart').addEventListener('click', moveDisc);
document.getElementById('towerMiddle').addEventListener('click', moveDisc);
document.getElementById('towerEnd').addEventListener('click', moveDisc);

function reset() {
    window.location.reload();
}




//My partner is James English and we got help from Coach David Camargo